<?php

namespace Ahs\RetailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ArticleController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AhsRetailBundle:Entities')->findAll();
        $categories = $em->getRepository('AhsRetailBundle:Categories')->findAll();


        return $this->render(
            'AhsRetailBundle:Article:index.html.twig',array(
            'entities' => $entities, 'categories' => $categories,

        ));
    }

}
