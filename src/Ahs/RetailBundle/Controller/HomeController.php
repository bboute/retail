<?php

namespace Ahs\RetailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('AhsRetailBundle:Categories')->findAll();



        return $this->render(
            'AhsRetailBundle:Home:index.html.twig',array(
            'categories' => $categories,


        ));

    }
}
