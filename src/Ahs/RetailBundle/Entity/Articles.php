<?php

namespace Ahs\RetailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Articles
 *
 * @ORM\Table(name="articles")
 * @ORM\Entity
 */
class Articles
{
    /**
     * @var string
     *
     * @ORM\Column(name="article_body", type="text", nullable=false)
     */
    private $articleBody;

    /**
     * @var \Ahs\RetailBundle\Entity\Entities
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Ahs\RetailBundle\Entity\Entities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_id", referencedColumnName="entity_id")
     * })
     */
    private $entity;



    /**
     * Set articleBody
     *
     * @param string $articleBody
     * @return Articles
     */
    public function setArticleBody($articleBody)
    {
        $this->articleBody = $articleBody;

        return $this;
    }

    /**
     * Get articleBody
     *
     * @return string 
     */
    public function getArticleBody()
    {
        return $this->articleBody;
    }

    /**
     * Set entity
     *
     * @param \Ahs\RetailBundle\Entity\Entities $entity
     * @return Articles
     */
    public function setEntity(\Ahs\RetailBundle\Entity\Entities $entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \Ahs\RetailBundle\Entity\Entities 
     */
    public function getEntity()
    {
        return $this->entity;
    }
}
