<?php

namespace Ahs\RetailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entities
 *
 * @ORM\Table(name="entities")
 * @ORM\Entity
 */
class Entities
{
    /**
     * @var string
     *
     * @ORM\Column(name="entity_title", type="string", length=255, nullable=false)
     */
    private $entityTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_description", type="text", nullable=false)
     */
    private $entityDescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="entity_created", type="datetime", nullable=false)
     */
    private $entityCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="entity_modified", type="datetime", nullable=true)
     */
    private $entityModified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="entity_deleted", type="datetime", nullable=true)
     */
    private $entityDeleted;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_price", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $entityPrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="entity_id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $entityId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Ahs\RetailBundle\Entity\Users", inversedBy="entity")
     * @ORM\JoinTable(name="entities_has_likes",
     *   joinColumns={
     *     @ORM\JoinColumn(name="entity_id", referencedColumnName="entity_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     *   }
     * )
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="Categories", inversedBy="entity", fetch="EAGER")
     * @ORM\JoinTable(name="entities_has_categories",
     *      joinColumns={@ORM\JoinColumn(name="entity_id", referencedColumnName="entity_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="category_id")}
     *      )
     */
    private $category;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set entityTitle
     *
     * @param string $entityTitle
     * @return Entities
     */
    public function setEntityTitle($entityTitle)
    {
        $this->entityTitle = $entityTitle;

        return $this;
    }

    /**
     * Get entityTitle
     *
     * @return string 
     */
    public function getEntityTitle()
    {
        return $this->entityTitle;
    }

    /**
     * Set entityDescription
     *
     * @param string $entityDescription
     * @return Entities
     */
    public function setEntityDescription($entityDescription)
    {
        $this->entityDescription = $entityDescription;

        return $this;
    }

    /**
     * Get entityDescription
     *
     * @return string 
     */
    public function getEntityDescription()
    {
        return $this->entityDescription;
    }

    /**
     * Set entityCreated
     *
     * @param \DateTime $entityCreated
     * @return Entities
     */
    public function setEntityCreated($entityCreated)
    {
        $this->entityCreated = $entityCreated;

        return $this;
    }

    /**
     * Get entityCreated
     *
     * @return \DateTime 
     */
    public function getEntityCreated()
    {
        return $this->entityCreated;
    }

    /**
     * Set entityModified
     *
     * @param \DateTime $entityModified
     * @return Entities
     */
    public function setEntityModified($entityModified)
    {
        $this->entityModified = $entityModified;

        return $this;
    }

    /**
     * Get entityModified
     *
     * @return \DateTime 
     */
    public function getEntityModified()
    {
        return $this->entityModified;
    }

    /**
     * Set entityDeleted
     *
     * @param \DateTime $entityDeleted
     * @return Entities
     */
    public function setEntityDeleted($entityDeleted)
    {
        $this->entityDeleted = $entityDeleted;

        return $this;
    }

    /**
     * Get entityDeleted
     *
     * @return \DateTime 
     */
    public function getEntityDeleted()
    {
        return $this->entityDeleted;
    }

    /**
     * Set entityPrice
     *
     * @param string $entityPrice
     * @return Entities
     */
    public function setEntityPrice($entityPrice)
    {
        $this->entityPrice = $entityPrice;

        return $this;
    }

    /**
     * Get entityPrice
     *
     * @return string 
     */
    public function getEntityPrice()
    {
        return $this->entityPrice;
    }

    /**
     * Get entityId
     *
     * @return integer 
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Add user
     *
     * @param \Ahs\RetailBundle\Entity\Users $user
     * @return Entities
     */
    public function addUser(\Ahs\RetailBundle\Entity\Users $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Ahs\RetailBundle\Entity\Users $user
     */
    public function removeUser(\Ahs\RetailBundle\Entity\Users $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add category
     *
     * @param \Ahs\RetailBundle\Entity\Categories $category
     * @return Entities
     */
    public function addCategory(\Ahs\RetailBundle\Entity\Categories $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \Ahs\RetailBundle\Entity\Categories $category
     */
    public function removeCategory(\Ahs\RetailBundle\Entity\Categories $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategory()
    {
        return $this->category;
    }

}
