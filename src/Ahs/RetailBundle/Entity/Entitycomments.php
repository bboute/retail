<?php

namespace Ahs\RetailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entitycomments
 *
 * @ORM\Table(name="entitycomments", indexes={@ORM\Index(name="fk_comments_users1_idx", columns={"user_id"}), @ORM\Index(name="fk_comments_entities1_idx", columns={"entity_id"}), @ORM\Index(name="fk_entitycomments_entitycomments1_idx", columns={"comment_parentid"})})
 * @ORM\Entity
 */
class Entitycomments
{
    /**
     * @var string
     *
     * @ORM\Column(name="comment_body", type="text", nullable=false)
     */
    private $commentBody;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="comment_created", type="datetime", nullable=false)
     */
    private $commentCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="comment_modified", type="datetime", nullable=true)
     */
    private $commentModified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="comment_deleted", type="datetime", nullable=true)
     */
    private $commentDeleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="comment_locked", type="datetime", nullable=true)
     */
    private $commentLocked;

    /**
     * @var integer
     *
     * @ORM\Column(name="comment_id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $commentId;

    /**
     * @var \Ahs\RetailBundle\Entity\Entitycomments
     *
     * @ORM\ManyToOne(targetEntity="Ahs\RetailBundle\Entity\Entitycomments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="comment_parentid", referencedColumnName="comment_id")
     * })
     */
    private $commentParentid;

    /**
     * @var \Ahs\RetailBundle\Entity\Entities
     *
     * @ORM\ManyToOne(targetEntity="Ahs\RetailBundle\Entity\Entities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_id", referencedColumnName="entity_id")
     * })
     */
    private $entity;

    /**
     * @var \Ahs\RetailBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="Ahs\RetailBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $user;



    /**
     * Set commentBody
     *
     * @param string $commentBody
     * @return Entitycomments
     */
    public function setCommentBody($commentBody)
    {
        $this->commentBody = $commentBody;

        return $this;
    }

    /**
     * Get commentBody
     *
     * @return string 
     */
    public function getCommentBody()
    {
        return $this->commentBody;
    }

    /**
     * Set commentCreated
     *
     * @param \DateTime $commentCreated
     * @return Entitycomments
     */
    public function setCommentCreated($commentCreated)
    {
        $this->commentCreated = $commentCreated;

        return $this;
    }

    /**
     * Get commentCreated
     *
     * @return \DateTime 
     */
    public function getCommentCreated()
    {
        return $this->commentCreated;
    }

    /**
     * Set commentModified
     *
     * @param \DateTime $commentModified
     * @return Entitycomments
     */
    public function setCommentModified($commentModified)
    {
        $this->commentModified = $commentModified;

        return $this;
    }

    /**
     * Get commentModified
     *
     * @return \DateTime 
     */
    public function getCommentModified()
    {
        return $this->commentModified;
    }

    /**
     * Set commentDeleted
     *
     * @param \DateTime $commentDeleted
     * @return Entitycomments
     */
    public function setCommentDeleted($commentDeleted)
    {
        $this->commentDeleted = $commentDeleted;

        return $this;
    }

    /**
     * Get commentDeleted
     *
     * @return \DateTime 
     */
    public function getCommentDeleted()
    {
        return $this->commentDeleted;
    }

    /**
     * Set commentLocked
     *
     * @param \DateTime $commentLocked
     * @return Entitycomments
     */
    public function setCommentLocked($commentLocked)
    {
        $this->commentLocked = $commentLocked;

        return $this;
    }

    /**
     * Get commentLocked
     *
     * @return \DateTime 
     */
    public function getCommentLocked()
    {
        return $this->commentLocked;
    }

    /**
     * Get commentId
     *
     * @return integer 
     */
    public function getCommentId()
    {
        return $this->commentId;
    }

    /**
     * Set commentParentid
     *
     * @param \Ahs\RetailBundle\Entity\Entitycomments $commentParentid
     * @return Entitycomments
     */
    public function setCommentParentid(\Ahs\RetailBundle\Entity\Entitycomments $commentParentid = null)
    {
        $this->commentParentid = $commentParentid;

        return $this;
    }

    /**
     * Get commentParentid
     *
     * @return \Ahs\RetailBundle\Entity\Entitycomments 
     */
    public function getCommentParentid()
    {
        return $this->commentParentid;
    }

    /**
     * Set entity
     *
     * @param \Ahs\RetailBundle\Entity\Entities $entity
     * @return Entitycomments
     */
    public function setEntity(\Ahs\RetailBundle\Entity\Entities $entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \Ahs\RetailBundle\Entity\Entities 
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set user
     *
     * @param \Ahs\RetailBundle\Entity\Users $user
     * @return Entitycomments
     */
    public function setUser(\Ahs\RetailBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Ahs\RetailBundle\Entity\Users 
     */
    public function getUser()
    {
        return $this->user;
    }
}
