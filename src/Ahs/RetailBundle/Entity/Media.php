<?php

namespace Ahs\RetailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Media
 *
 * @ORM\Table(name="media")
 * @ORM\Entity
 */
class Media
{
    /**
     * @var string
     *
     * @ORM\Column(name="medium_url", type="string", length=255, nullable=false)
     */
    private $mediumUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="medium_type", type="string", length=3, nullable=false)
     */
    private $mediumType;

    /**
     * @var string
     *
     * @ORM\Column(name="medium_mimetype", type="string", length=255, nullable=false)
     */
    private $mediumMimetype;

    /**
     * @var boolean
     *
     * @ORM\Column(name="medium_isexternal", type="boolean", nullable=true)
     */
    private $mediumIsexternal;

    /**
     * @var \Ahs\RetailBundle\Entity\Entities
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Ahs\RetailBundle\Entity\Entities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_id", referencedColumnName="entity_id")
     * })
     */
    private $entity;



    /**
     * Set mediumUrl
     *
     * @param string $mediumUrl
     * @return Media
     */
    public function setMediumUrl($mediumUrl)
    {
        $this->mediumUrl = $mediumUrl;

        return $this;
    }

    /**
     * Get mediumUrl
     *
     * @return string 
     */
    public function getMediumUrl()
    {
        return $this->mediumUrl;
    }

    /**
     * Set mediumType
     *
     * @param string $mediumType
     * @return Media
     */
    public function setMediumType($mediumType)
    {
        $this->mediumType = $mediumType;

        return $this;
    }

    /**
     * Get mediumType
     *
     * @return string 
     */
    public function getMediumType()
    {
        return $this->mediumType;
    }

    /**
     * Set mediumMimetype
     *
     * @param string $mediumMimetype
     * @return Media
     */
    public function setMediumMimetype($mediumMimetype)
    {
        $this->mediumMimetype = $mediumMimetype;

        return $this;
    }

    /**
     * Get mediumMimetype
     *
     * @return string 
     */
    public function getMediumMimetype()
    {
        return $this->mediumMimetype;
    }

    /**
     * Set mediumIsexternal
     *
     * @param boolean $mediumIsexternal
     * @return Media
     */
    public function setMediumIsexternal($mediumIsexternal)
    {
        $this->mediumIsexternal = $mediumIsexternal;

        return $this;
    }

    /**
     * Get mediumIsexternal
     *
     * @return boolean 
     */
    public function getMediumIsexternal()
    {
        return $this->mediumIsexternal;
    }

    /**
     * Set entity
     *
     * @param \Ahs\RetailBundle\Entity\Entities $entity
     * @return Media
     */
    public function setEntity(\Ahs\RetailBundle\Entity\Entities $entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return \Ahs\RetailBundle\Entity\Entities 
     */
    public function getEntity()
    {
        return $this->entity;
    }
}
