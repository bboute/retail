<?php

namespace Ahs\RetailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Members
 *
 * @ORM\Table(name="members")
 * @ORM\Entity
 */
class Members
{
    /**
     * @var string
     *
     * @ORM\Column(name="member_password", type="string", length=60, nullable=false)
     */
    private $memberPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="member_salt", type="string", length=30, nullable=false)
     */
    private $memberSalt;

    /**
     * @var string
     *
     * @ORM\Column(name="member_token", type="string", length=128, nullable=true)
     */
    private $memberToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="member_confirmed", type="datetime", nullable=true)
     */
    private $memberConfirmed;

    /**
     * @var \Ahs\RetailBundle\Entity\Users
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Ahs\RetailBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $user;



    /**
     * Set memberPassword
     *
     * @param string $memberPassword
     * @return Members
     */
    public function setMemberPassword($memberPassword)
    {
        $this->memberPassword = $memberPassword;

        return $this;
    }

    /**
     * Get memberPassword
     *
     * @return string 
     */
    public function getMemberPassword()
    {
        return $this->memberPassword;
    }

    /**
     * Set memberSalt
     *
     * @param string $memberSalt
     * @return Members
     */
    public function setMemberSalt($memberSalt)
    {
        $this->memberSalt = $memberSalt;

        return $this;
    }

    /**
     * Get memberSalt
     *
     * @return string 
     */
    public function getMemberSalt()
    {
        return $this->memberSalt;
    }

    /**
     * Set memberToken
     *
     * @param string $memberToken
     * @return Members
     */
    public function setMemberToken($memberToken)
    {
        $this->memberToken = $memberToken;

        return $this;
    }

    /**
     * Get memberToken
     *
     * @return string 
     */
    public function getMemberToken()
    {
        return $this->memberToken;
    }

    /**
     * Set memberConfirmed
     *
     * @param \DateTime $memberConfirmed
     * @return Members
     */
    public function setMemberConfirmed($memberConfirmed)
    {
        $this->memberConfirmed = $memberConfirmed;

        return $this;
    }

    /**
     * Get memberConfirmed
     *
     * @return \DateTime 
     */
    public function getMemberConfirmed()
    {
        return $this->memberConfirmed;
    }

    /**
     * Set user
     *
     * @param \Ahs\RetailBundle\Entity\Users $user
     * @return Members
     */
    public function setUser(\Ahs\RetailBundle\Entity\Users $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Ahs\RetailBundle\Entity\Users 
     */
    public function getUser()
    {
        return $this->user;
    }
}
