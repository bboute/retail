<?php

namespace Ahs\RetailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Messages
 *
 * @ORM\Table(name="messages", indexes={@ORM\Index(name="fk_messages_users1_idx", columns={"user_id"}), @ORM\Index(name="fk_messages_users2_idx", columns={"receiver_id"})})
 * @ORM\Entity
 */
class Messages
{
    /**
     * @var string
     *
     * @ORM\Column(name="message_body", type="text", nullable=false)
     */
    private $messageBody;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="message_created", type="datetime", nullable=false)
     */
    private $messageCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="message_deleted", type="datetime", nullable=true)
     */
    private $messageDeleted;

    /**
     * @var string
     *
     * @ORM\Column(name="message_id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $messageId;

    /**
     * @var \Ahs\RetailBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="Ahs\RetailBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="receiver_id", referencedColumnName="user_id")
     * })
     */
    private $receiver;

    /**
     * @var \Ahs\RetailBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="Ahs\RetailBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $user;



    /**
     * Set messageBody
     *
     * @param string $messageBody
     * @return Messages
     */
    public function setMessageBody($messageBody)
    {
        $this->messageBody = $messageBody;

        return $this;
    }

    /**
     * Get messageBody
     *
     * @return string 
     */
    public function getMessageBody()
    {
        return $this->messageBody;
    }

    /**
     * Set messageCreated
     *
     * @param \DateTime $messageCreated
     * @return Messages
     */
    public function setMessageCreated($messageCreated)
    {
        $this->messageCreated = $messageCreated;

        return $this;
    }

    /**
     * Get messageCreated
     *
     * @return \DateTime 
     */
    public function getMessageCreated()
    {
        return $this->messageCreated;
    }

    /**
     * Set messageDeleted
     *
     * @param \DateTime $messageDeleted
     * @return Messages
     */
    public function setMessageDeleted($messageDeleted)
    {
        $this->messageDeleted = $messageDeleted;

        return $this;
    }

    /**
     * Get messageDeleted
     *
     * @return \DateTime 
     */
    public function getMessageDeleted()
    {
        return $this->messageDeleted;
    }

    /**
     * Get messageId
     *
     * @return string 
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * Set receiver
     *
     * @param \Ahs\RetailBundle\Entity\Users $receiver
     * @return Messages
     */
    public function setReceiver(\Ahs\RetailBundle\Entity\Users $receiver = null)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return \Ahs\RetailBundle\Entity\Users 
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set user
     *
     * @param \Ahs\RetailBundle\Entity\Users $user
     * @return Messages
     */
    public function setUser(\Ahs\RetailBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Ahs\RetailBundle\Entity\Users 
     */
    public function getUser()
    {
        return $this->user;
    }
}
