<?php

namespace Ahs\RetailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Persons
 *
 * @ORM\Table(name="persons")
 * @ORM\Entity
 */
class Persons
{
    /**
     * @var string
     *
     * @ORM\Column(name="person_firstname", type="string", length=45, nullable=false)
     */
    private $personFirstname;

    /**
     * @var string
     *
     * @ORM\Column(name="person_surname", type="string", length=255, nullable=false)
     */
    private $personSurname;

    /**
     * @var string
     *
     * @ORM\Column(name="person_type", type="string", length=16, nullable=false)
     */
    private $personType;

    /**
     * @var string
     *
     * @ORM\Column(name="person_profile", type="text", nullable=true)
     */
    private $personProfile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="person_created", type="datetime", nullable=false)
     */
    private $personCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="person_modified", type="datetime", nullable=true)
     */
    private $personModified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="person_deleted", type="datetime", nullable=true)
     */
    private $personDeleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personId;



    /**
     * Set personFirstname
     *
     * @param string $personFirstname
     * @return Persons
     */
    public function setPersonFirstname($personFirstname)
    {
        $this->personFirstname = $personFirstname;

        return $this;
    }

    /**
     * Get personFirstname
     *
     * @return string 
     */
    public function getPersonFirstname()
    {
        return $this->personFirstname;
    }

    /**
     * Set personSurname
     *
     * @param string $personSurname
     * @return Persons
     */
    public function setPersonSurname($personSurname)
    {
        $this->personSurname = $personSurname;

        return $this;
    }

    /**
     * Get personSurname
     *
     * @return string 
     */
    public function getPersonSurname()
    {
        return $this->personSurname;
    }

    /**
     * Set personType
     *
     * @param string $personType
     * @return Persons
     */
    public function setPersonType($personType)
    {
        $this->personType = $personType;

        return $this;
    }

    /**
     * Get personType
     *
     * @return string 
     */
    public function getPersonType()
    {
        return $this->personType;
    }

    /**
     * Set personProfile
     *
     * @param string $personProfile
     * @return Persons
     */
    public function setPersonProfile($personProfile)
    {
        $this->personProfile = $personProfile;

        return $this;
    }

    /**
     * Get personProfile
     *
     * @return string 
     */
    public function getPersonProfile()
    {
        return $this->personProfile;
    }

    /**
     * Set personCreated
     *
     * @param \DateTime $personCreated
     * @return Persons
     */
    public function setPersonCreated($personCreated)
    {
        $this->personCreated = $personCreated;

        return $this;
    }

    /**
     * Get personCreated
     *
     * @return \DateTime 
     */
    public function getPersonCreated()
    {
        return $this->personCreated;
    }

    /**
     * Set personModified
     *
     * @param \DateTime $personModified
     * @return Persons
     */
    public function setPersonModified($personModified)
    {
        $this->personModified = $personModified;

        return $this;
    }

    /**
     * Get personModified
     *
     * @return \DateTime 
     */
    public function getPersonModified()
    {
        return $this->personModified;
    }

    /**
     * Set personDeleted
     *
     * @param \DateTime $personDeleted
     * @return Persons
     */
    public function setPersonDeleted($personDeleted)
    {
        $this->personDeleted = $personDeleted;

        return $this;
    }

    /**
     * Get personDeleted
     *
     * @return \DateTime 
     */
    public function getPersonDeleted()
    {
        return $this->personDeleted;
    }

    /**
     * Get personId
     *
     * @return integer 
     */
    public function getPersonId()
    {
        return $this->personId;
    }
}
