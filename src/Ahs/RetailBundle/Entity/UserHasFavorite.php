<?php

namespace Ahs\RetailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserHasFavorite
 *
 * @ORM\Table(name="user_has_favorite", indexes={@ORM\Index(name="fk_users_has_friendrequests_users1_idx", columns={"user_id"}), @ORM\Index(name="fk_users_has_friendrequests_users2_idx", columns={"favoriteuser_id"})})
 * @ORM\Entity
 */
class UserHasFavorite
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="uhf_created", type="datetime", nullable=false)
     */
    private $uhfCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="uhf_accepted", type="datetime", nullable=true)
     */
    private $uhfAccepted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="uhf_rejected", type="datetime", nullable=true)
     */
    private $uhfRejected;

    /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer", nullable=false)
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="uhf_id", type="string", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uhfId;

    /**
     * @var \Ahs\RetailBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="Ahs\RetailBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="favoriteuser_id", referencedColumnName="user_id")
     * })
     */
    private $favoriteuser;

    /**
     * @var \Ahs\RetailBundle\Entity\Users
     *
     * @ORM\ManyToOne(targetEntity="Ahs\RetailBundle\Entity\Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $user;



    /**
     * Set uhfCreated
     *
     * @param \DateTime $uhfCreated
     * @return UserHasFavorite
     */
    public function setUhfCreated($uhfCreated)
    {
        $this->uhfCreated = $uhfCreated;

        return $this;
    }

    /**
     * Get uhfCreated
     *
     * @return \DateTime 
     */
    public function getUhfCreated()
    {
        return $this->uhfCreated;
    }

    /**
     * Set uhfAccepted
     *
     * @param \DateTime $uhfAccepted
     * @return UserHasFavorite
     */
    public function setUhfAccepted($uhfAccepted)
    {
        $this->uhfAccepted = $uhfAccepted;

        return $this;
    }

    /**
     * Get uhfAccepted
     *
     * @return \DateTime 
     */
    public function getUhfAccepted()
    {
        return $this->uhfAccepted;
    }

    /**
     * Set uhfRejected
     *
     * @param \DateTime $uhfRejected
     * @return UserHasFavorite
     */
    public function setUhfRejected($uhfRejected)
    {
        $this->uhfRejected = $uhfRejected;

        return $this;
    }

    /**
     * Get uhfRejected
     *
     * @return \DateTime 
     */
    public function getUhfRejected()
    {
        return $this->uhfRejected;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return UserHasFavorite
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return UserHasFavorite
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Get uhfId
     *
     * @return string 
     */
    public function getUhfId()
    {
        return $this->uhfId;
    }

    /**
     * Set favoriteuser
     *
     * @param \Ahs\RetailBundle\Entity\Users $favoriteuser
     * @return UserHasFavorite
     */
    public function setFavoriteuser(\Ahs\RetailBundle\Entity\Users $favoriteuser = null)
    {
        $this->favoriteuser = $favoriteuser;

        return $this;
    }

    /**
     * Get favoriteuser
     *
     * @return \Ahs\RetailBundle\Entity\Users 
     */
    public function getFavoriteuser()
    {
        return $this->favoriteuser;
    }

    /**
     * Set user
     *
     * @param \Ahs\RetailBundle\Entity\Users $user
     * @return UserHasFavorite
     */
    public function setUser(\Ahs\RetailBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Ahs\RetailBundle\Entity\Users 
     */
    public function getUser()
    {
        return $this->user;
    }
}
