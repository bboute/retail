<?php

namespace Ahs\RetailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="user_username_UNIQUE", columns={"user_username"}), @ORM\UniqueConstraint(name="user_email_UNIQUE", columns={"user_email"})}, indexes={@ORM\Index(name="fk_users_persons1", columns={"person_id"})})
 * @ORM\Entity
 */
class Users
{
    /**
     * @var string
     *
     * @ORM\Column(name="user_username", type="string", length=45, nullable=false)
     */
    private $userUsername;

    /**
     * @var string
     *
     * @ORM\Column(name="user_email", type="string", length=255, nullable=false)
     */
    private $userEmail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_created", type="datetime", nullable=false)
     */
    private $userCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_modified", type="datetime", nullable=true)
     */
    private $userModified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_deleted", type="datetime", nullable=true)
     */
    private $userDeleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_lastlogin", type="datetime", nullable=true)
     */
    private $userLastlogin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="user_locked", type="datetime", nullable=true)
     */
    private $userLocked;

    /**
     * @var string
     *
     * @ORM\Column(name="user_avatar", type="string", length=255, nullable=true)
     */
    private $userAvatar;

    /**
     * @var boolean
     *
     * @ORM\Column(name="user_avatar_isexternal", type="boolean", nullable=true)
     */
    private $userAvatarIsexternal;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var \Ahs\RetailBundle\Entity\Persons
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Ahs\RetailBundle\Entity\Persons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
     * })
     */
    private $person;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Ahs\RetailBundle\Entity\Roles", inversedBy="user")
     * @ORM\JoinTable(name="users_has_roles",
     *   joinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="role_id", referencedColumnName="role_id")
     *   }
     * )
     */
    private $role;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Ahs\RetailBundle\Entity\Entities", mappedBy="user")
     */
    private $entity;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->role = new \Doctrine\Common\Collections\ArrayCollection();
        $this->entity = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set userUsername
     *
     * @param string $userUsername
     * @return Users
     */
    public function setUserUsername($userUsername)
    {
        $this->userUsername = $userUsername;

        return $this;
    }

    /**
     * Get userUsername
     *
     * @return string 
     */
    public function getUserUsername()
    {
        return $this->userUsername;
    }

    /**
     * Set userEmail
     *
     * @param string $userEmail
     * @return Users
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * Get userEmail
     *
     * @return string 
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * Set userCreated
     *
     * @param \DateTime $userCreated
     * @return Users
     */
    public function setUserCreated($userCreated)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \DateTime 
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \DateTime $userModified
     * @return Users
     */
    public function setUserModified($userModified)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \DateTime 
     */
    public function getUserModified()
    {
        return $this->userModified;
    }

    /**
     * Set userDeleted
     *
     * @param \DateTime $userDeleted
     * @return Users
     */
    public function setUserDeleted($userDeleted)
    {
        $this->userDeleted = $userDeleted;

        return $this;
    }

    /**
     * Get userDeleted
     *
     * @return \DateTime 
     */
    public function getUserDeleted()
    {
        return $this->userDeleted;
    }

    /**
     * Set userLastlogin
     *
     * @param \DateTime $userLastlogin
     * @return Users
     */
    public function setUserLastlogin($userLastlogin)
    {
        $this->userLastlogin = $userLastlogin;

        return $this;
    }

    /**
     * Get userLastlogin
     *
     * @return \DateTime 
     */
    public function getUserLastlogin()
    {
        return $this->userLastlogin;
    }

    /**
     * Set userLocked
     *
     * @param \DateTime $userLocked
     * @return Users
     */
    public function setUserLocked($userLocked)
    {
        $this->userLocked = $userLocked;

        return $this;
    }

    /**
     * Get userLocked
     *
     * @return \DateTime 
     */
    public function getUserLocked()
    {
        return $this->userLocked;
    }

    /**
     * Set userAvatar
     *
     * @param string $userAvatar
     * @return Users
     */
    public function setUserAvatar($userAvatar)
    {
        $this->userAvatar = $userAvatar;

        return $this;
    }

    /**
     * Get userAvatar
     *
     * @return string 
     */
    public function getUserAvatar()
    {
        return $this->userAvatar;
    }

    /**
     * Set userAvatarIsexternal
     *
     * @param boolean $userAvatarIsexternal
     * @return Users
     */
    public function setUserAvatarIsexternal($userAvatarIsexternal)
    {
        $this->userAvatarIsexternal = $userAvatarIsexternal;

        return $this;
    }

    /**
     * Get userAvatarIsexternal
     *
     * @return boolean 
     */
    public function getUserAvatarIsexternal()
    {
        return $this->userAvatarIsexternal;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Users
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set person
     *
     * @param \Ahs\RetailBundle\Entity\Persons $person
     * @return Users
     */
    public function setPerson(\Ahs\RetailBundle\Entity\Persons $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \Ahs\RetailBundle\Entity\Persons 
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Add role
     *
     * @param \Ahs\RetailBundle\Entity\Roles $role
     * @return Users
     */
    public function addRole(\Ahs\RetailBundle\Entity\Roles $role)
    {
        $this->role[] = $role;

        return $this;
    }

    /**
     * Remove role
     *
     * @param \Ahs\RetailBundle\Entity\Roles $role
     */
    public function removeRole(\Ahs\RetailBundle\Entity\Roles $role)
    {
        $this->role->removeElement($role);
    }

    /**
     * Get role
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add entity
     *
     * @param \Ahs\RetailBundle\Entity\Entities $entity
     * @return Users
     */
    public function addEntity(\Ahs\RetailBundle\Entity\Entities $entity)
    {
        $this->entity[] = $entity;

        return $this;
    }

    /**
     * Remove entity
     *
     * @param \Ahs\RetailBundle\Entity\Entities $entity
     */
    public function removeEntity(\Ahs\RetailBundle\Entity\Entities $entity)
    {
        $this->entity->removeElement($entity);
    }

    /**
     * Get entity
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEntity()
    {
        return $this->entity;
    }
}
